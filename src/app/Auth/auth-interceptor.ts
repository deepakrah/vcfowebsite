import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private _router: Router
        ) {
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // 
        // add authorization header with jwt token if available      


        let Token = localStorage.getItem('token');

        if (localStorage.getItem('token') != null) {
            const clonedReq = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${Token}`
                }
            });

            return next.handle(clonedReq).pipe(
                tap(
                    succ => { },
                    err => {
                        if (err.status == 401) {
                            // this._LocalStorage.removeAllStoreOnLocalStorage();
                            localStorage.removeItem("userId");
                            localStorage.removeItem("token");
                            // this._router.navigateByUrl('/Home');
                        }
                    }
                )
            );
        }
        else {
            // this._LocalStorage.removeAllStoreOnLocalStorage();
            localStorage.removeItem("userId");
            localStorage.removeItem("token");
            // this._router.navigateByUrl('/Home');
            return next.handle(req.clone());

        }
    }
}

