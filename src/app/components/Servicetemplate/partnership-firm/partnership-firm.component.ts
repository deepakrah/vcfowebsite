import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-partnership-firm',
  templateUrl: './partnership-firm.component.html',
  styleUrls: ['./partnership-firm.component.css']
})
export class PartnershipFirmComponent implements OnInit {

  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '305'
    };
    debugger
    localStorage.setItem('servicecode', "305");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }

}
