import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-copyright-registration',
  templateUrl: './copyright-registration.component.html',
  styleUrls: ['./copyright-registration.component.css']
})
export class CopyrightRegistrationComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '401'
    };
    debugger
    localStorage.setItem('servicecode', "401");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }
}
