import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-fssairegistration',
  templateUrl: './fssairegistration.component.html',
  styleUrls: ['./fssairegistration.component.css']
})
export class FSSAIRegistrationComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '306'
    };
    debugger
    localStorage.setItem('servicecode', "306");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }
}
