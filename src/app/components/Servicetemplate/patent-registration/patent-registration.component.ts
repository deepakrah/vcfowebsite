import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-patent-registration',
  templateUrl: './patent-registration.component.html',
  styleUrls: ['./patent-registration.component.css']
})
export class PatentRegistrationComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '402'
    };
    debugger
    localStorage.setItem('servicecode', "402");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
    });
  }
}
