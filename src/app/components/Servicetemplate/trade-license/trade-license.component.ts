import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-trade-license',
  templateUrl: './trade-license.component.html',
  styleUrls: ['./trade-license.component.css']
})
export class TradeLicenseComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '310'
    };
    debugger
    localStorage.setItem('servicecode', "310");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }

}
