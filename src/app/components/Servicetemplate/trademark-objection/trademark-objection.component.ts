import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-trademark-objection',
  templateUrl: './trademark-objection.component.html',
  styleUrls: ['./trademark-objection.component.css']
})
export class TrademarkObjectionComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '405'
    };
    debugger
    localStorage.setItem('servicecode', "405");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
      this.spinner.hide();

    });
  }
}
