import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTRegistrationComponent } from './gstregistration.component';

describe('GSTRegistrationComponent', () => {
  let component: GSTRegistrationComponent;
  let fixture: ComponentFixture<GSTRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSTRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
