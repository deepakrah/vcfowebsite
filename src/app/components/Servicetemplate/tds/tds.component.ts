import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-tds',
  templateUrl: './tds.component.html',
  styleUrls: ['./tds.component.css']
})
export class TDSComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '203'
    };
    debugger
    localStorage.setItem('servicecode', "203");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
      this.spinner.hide();

    });
  }

}
