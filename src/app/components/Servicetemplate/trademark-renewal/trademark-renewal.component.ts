import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-trademark-renewal',
  templateUrl: './trademark-renewal.component.html',
  styleUrls: ['./trademark-renewal.component.css']
})
export class TrademarkRenewalComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '407'
    };
    debugger
    localStorage.setItem('servicecode', "407");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
      this.spinner.hide();

    });
  }
}
