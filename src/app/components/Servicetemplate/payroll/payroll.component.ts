import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.css']
})
export class PayrollComponent implements OnInit {

  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '307'
    };
    debugger
    localStorage.setItem('servicecode', "307");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }

}
