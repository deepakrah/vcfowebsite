import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-trademark-opposition',
  templateUrl: './trademark-opposition.component.html',
  styleUrls: ['./trademark-opposition.component.css']
})
export class TrademarkOppositionComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '406'
    };
    debugger
    localStorage.setItem('servicecode', "406");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
      this.spinner.hide();

    });
  }

}
