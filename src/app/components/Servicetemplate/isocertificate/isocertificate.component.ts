import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-isocertificate',
  templateUrl: './isocertificate.component.html',
  styleUrls: ['./isocertificate.component.css']
})
export class ISOCertificateComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '309'
    };
    debugger
    localStorage.setItem('servicecode', "309");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }

}
