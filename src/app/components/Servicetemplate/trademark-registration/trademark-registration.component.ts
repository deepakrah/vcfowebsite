import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-trademark-registration',
  templateUrl: './trademark-registration.component.html',
  styleUrls: ['./trademark-registration.component.css']
})
export class TrademarkRegistrationComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '404'
    };
    debugger
    localStorage.setItem('servicecode', "404");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
    
      this.price = res[0].price;
      this.spinner.hide();

    });
  }
}
