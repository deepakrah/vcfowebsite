import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-gstinvoicing',
  templateUrl: './gstinvoicing.component.html',
  styleUrls: ['./gstinvoicing.component.css']
})
export class GSTInvoicingComponent implements OnInit {
  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '307'
    };
    debugger
    localStorage.setItem('servicecode', "307");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }

}
