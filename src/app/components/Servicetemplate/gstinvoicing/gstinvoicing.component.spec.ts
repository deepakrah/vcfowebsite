import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTInvoicingComponent } from './gstinvoicing.component';

describe('GSTInvoicingComponent', () => {
  let component: GSTInvoicingComponent;
  let fixture: ComponentFixture<GSTInvoicingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSTInvoicingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTInvoicingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
