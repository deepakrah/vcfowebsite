import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-provisional-patent',
  templateUrl: './provisional-patent.component.html',
  styleUrls: ['./provisional-patent.component.css']
})
export class ProvisionalPatentComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '403'
    };
    debugger
    localStorage.setItem('servicecode', "403");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }

}
