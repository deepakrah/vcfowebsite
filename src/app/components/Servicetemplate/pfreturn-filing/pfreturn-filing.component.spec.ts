import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PFReturnFilingComponent } from './pfreturn-filing.component';

describe('PFReturnFilingComponent', () => {
  let component: PFReturnFilingComponent;
  let fixture: ComponentFixture<PFReturnFilingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PFReturnFilingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PFReturnFilingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
