import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-sole-proprietorship',
  templateUrl: './sole-proprietorship.component.html',
  styleUrls: ['./sole-proprietorship.component.css']
})
export class SoleProprietorshipComponent implements OnInit {

  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '304'
    };
    debugger
    localStorage.setItem('servicecode', "304");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }
}
