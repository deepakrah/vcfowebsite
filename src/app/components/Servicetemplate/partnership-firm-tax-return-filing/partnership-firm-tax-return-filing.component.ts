import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-partnership-firm-tax-return-filing',
  templateUrl: './partnership-firm-tax-return-filing.component.html',
  styleUrls: ['./partnership-firm-tax-return-filing.component.css']
})
export class PartnershipFirmTaxReturnFilingComponent implements OnInit {
  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '201'
    };
    debugger
    localStorage.setItem('servicecode', "201");
    this.spinner.show();
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.spinner.hide();
      this.price = res[0].price;
    });
  }
}
