import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-proprietorship-return-filing',
  templateUrl: './proprietorship-return-filing.component.html',
  styleUrls: ['./proprietorship-return-filing.component.css']
})
export class ProprietorshipReturnFilingComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
   
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '202'
    };
    debugger
    localStorage.setItem('servicecode', "202");
    this.spinner.show();
	
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
     
      this.price = res[0].price;
      this.spinner.hide();
	
    });
  }

}
