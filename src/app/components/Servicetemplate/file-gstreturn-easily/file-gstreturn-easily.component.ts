import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-file-gstreturn-easily',
  templateUrl: './file-gstreturn-easily.component.html',
  styleUrls: ['./file-gstreturn-easily.component.css']
})
export class FileGSTReturnEasilyComponent implements OnInit {

  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '307'
    };
    debugger
    localStorage.setItem('servicecode', "307");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }
}
