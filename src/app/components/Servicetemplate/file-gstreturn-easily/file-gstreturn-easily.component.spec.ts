import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileGSTReturnEasilyComponent } from './file-gstreturn-easily.component';

describe('FileGSTReturnEasilyComponent', () => {
  let component: FileGSTReturnEasilyComponent;
  let fixture: ComponentFixture<FileGSTReturnEasilyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FileGSTReturnEasilyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileGSTReturnEasilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
