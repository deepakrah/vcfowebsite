import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-private-limited-company',
  templateUrl: './private-limited-company.component.html',
  styleUrls: ['./private-limited-company.component.css']
})
export class PrivateLimitedCompanyComponent implements OnInit {
  name: any;
  price: any;
  constructor(
    private _servicesService: ServicesService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '301'
    };

    localStorage.setItem('servicecode', "301");
    debugger
    this.spinner.show();
    this._servicesService.GetServicesByCode(obj).subscribe(res => {

      this.price = res[0].price;
      this.spinner.hide();
    });
  }

}
