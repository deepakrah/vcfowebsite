import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTInputTaxCreditReconciliationComponent } from './gstinput-tax-credit-reconciliation.component';

describe('GSTInputTaxCreditReconciliationComponent', () => {
  let component: GSTInputTaxCreditReconciliationComponent;
  let fixture: ComponentFixture<GSTInputTaxCreditReconciliationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSTInputTaxCreditReconciliationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTInputTaxCreditReconciliationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
