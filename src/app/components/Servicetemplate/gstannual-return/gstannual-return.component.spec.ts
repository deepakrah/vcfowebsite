import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GSTAnnualReturnComponent } from './gstannual-return.component';

describe('GSTAnnualReturnComponent', () => {
  let component: GSTAnnualReturnComponent;
  let fixture: ComponentFixture<GSTAnnualReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GSTAnnualReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GSTAnnualReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
