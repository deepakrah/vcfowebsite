import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredOfficeChangeComponent } from './registered-office-change.component';

describe('RegisteredOfficeChangeComponent', () => {
  let component: RegisteredOfficeChangeComponent;
  let fixture: ComponentFixture<RegisteredOfficeChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisteredOfficeChangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredOfficeChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
