import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ESIRegistrationComponent } from './esiregistration.component';

describe('ESIRegistrationComponent', () => {
  let component: ESIRegistrationComponent;
  let fixture: ComponentFixture<ESIRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ESIRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ESIRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
