import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-limited-liability-partnership',
  templateUrl: './limited-liability-partnership.component.html',
  styleUrls: ['./limited-liability-partnership.component.css']
})
export class LimitedLiabilityPartnershipComponent implements OnInit {

  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '303'
    };
    debugger
    localStorage.setItem('servicecode', "303");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0]
    });
  }
}
