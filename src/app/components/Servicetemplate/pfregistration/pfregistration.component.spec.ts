import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PFRegistrationComponent } from './pfregistration.component';

describe('PFRegistrationComponent', () => {
  let component: PFRegistrationComponent;
  let fixture: ComponentFixture<PFRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PFRegistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PFRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
