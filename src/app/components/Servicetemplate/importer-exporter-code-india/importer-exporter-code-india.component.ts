import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-importer-exporter-code-india',
  templateUrl: './importer-exporter-code-india.component.html',
  styleUrls: ['./importer-exporter-code-india.component.css']
})
export class ImporterExporterCodeIndiaComponent implements OnInit {

  name: any;
  price:any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '308'
    };
    debugger
    localStorage.setItem('servicecode', "308");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
    });
  }
}
