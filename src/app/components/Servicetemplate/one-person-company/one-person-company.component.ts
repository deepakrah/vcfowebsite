import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-one-person-company',
  templateUrl: './one-person-company.component.html',
  styleUrls: ['./one-person-company.component.css']
})
export class OnePersonCompanyComponent implements OnInit {
  lst: any;
  constructor(
    private _servicesService: ServicesService
  ) { }

  ngOnInit(): void {
    let obj = {
      code: '302'
    };
    debugger
    localStorage.setItem('servicecode', "302");
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.lst = res[0];
    });
  }
}
