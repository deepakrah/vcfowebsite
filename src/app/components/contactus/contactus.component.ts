import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/shared/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ThisReceiver } from '@angular/compiler';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { UserService } from 'src/app/shared/services/user.service';

declare var Razorpay: any;
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  form: FormGroup;
  submitted: boolean = false;
  name: any;
  price: any;
  code: any;
  gst: number;
  TotalAmount: number;
  Username: string;
  email: string;
  phone: string;

  constructor(private _servicesService: ServicesService,
    private _userService: UserService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _datePipe: DatePipe,
    private router: Router) { }

  ngOnInit(): void {

    this.form = this.formBuilder.group({

      GSTNo: [''],
      BusinessName: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],

    });

    debugger
    let obj = {
      code: localStorage.getItem('servicecode')
    };

    this.Username = localStorage.getItem('Username');
    this.email = localStorage.getItem('email');
    this.phone = localStorage.getItem('phone');


    debugger
    this.spinner.show();
    this._servicesService.GetServicesByCode(obj).subscribe(res => {
      this.name = res[0].name;
      this.price = res[0].price;
      this.gst = Number(res[0].price * 18 / 100)
      this.TotalAmount = Number(res[0].price) + Number(res[0].price * 18 / 100)
      this.spinner.hide();
    });
  }

  get f() {
    return this.form.controls;
  }

  payNow() {

    this.submitted = true;
    if (this.form.invalid) {
      this.toastr.error('Please fill in all the * required fields.');
      return;
    }
    debugger
    var servicecode = parseInt(localStorage.getItem('servicecode'));
    var amount = this.TotalAmount
    var userId = parseInt(localStorage.getItem('UserId'));
    var orderNumber = this._datePipe.transform(new Date().toString(), 'yyyyMMddHHmmss');
    var model = {

      UserId: userId,
      ServiceCode: servicecode.toString(),
      orderNumber: orderNumber.toString(),
      order_date: this._datePipe.transform(new Date().toString(), 'yyyy-MM-dd HH:mm:ss'),
      GSTNo: this.form.value.GSTNo.toString(),
      BusinessName: this.form.value.BusinessName.toString(),
      state: this.form.value.state.toString(),
      country: this.form.value.country.toString(),
      pg_resStatus: '',
      pg_resCode: '',
      amount: Number(this.price),
      net_amount: Number(this.TotalAmount),
      tax: Number(this.gst)

    };

    var options = {
      "key": "rzp_test_dYcSSon3xxWzKA", // Enter the Key ID generated from the Dashboard
      "amount": amount, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
      "currency": "INR",
      "name": "Acme Corp",
      "description": "Test Transaction",
      "image": "",
      //"order_id": orderNumber, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
      // "handler": function (response) {
      //   debugger
      //   // alert(response.razorpay_payment_id);
      //   // alert(response.razorpay_order_id);
      //   // alert(response.razorpay_signature)
      //   console.log(response);

      // },
      handler: (e) => {
        debugger
        model.pg_resCode = e.razorpay_payment_id;
        model.pg_resStatus = 'success';
        debugger
        this._userService.addOrder(model).subscribe((d: any) => {
          debugger
          //this.router.navigate(['../final-resume/' + cvId]);
          // this.router.navigate(['/success']);
          window.open('/success','_parent');
        }, (err) => {
          debugger
        });
      },
      "prefill": {
        "name": this.Username,
        "email": this.email,
        "contact": this.phone
      },
      "notes": {
        "address": ""
      },
      "theme": {
        "color": "#F37254"
      }
    };
    var rzp = new Razorpay(options);
    rzp.open();
  }

}


