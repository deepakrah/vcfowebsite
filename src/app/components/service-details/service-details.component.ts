import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from 'src/app/shared/services/services.service';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.css']
})
export class ServiceDetailsComponent implements OnInit {
  rows: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _servicesService: ServicesService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      let obj = {
        id: Number(id)
      };
      this._servicesService.GetServicesById(obj).subscribe(res => {
        this.rows = res[0];
      });
    });
  }

}
