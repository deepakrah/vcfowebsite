import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public Mobile;
  public OTP;
  public User = [];
  LoggedInUserId: number;
  IsOtpSend = false;
  constructor(
    private _UserService: UserService,
    private _ToastrService: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
  }

  Login() {
    // this.router.navigate(['/Home']);
    debugger
    if (this.OTP.length < 5) {
      this._ToastrService.error('please enter 6 digit OTP');
      return false;
    }
    let d = {
      Mobile: this.Mobile,
      OTP: this.OTP
    };
    this.spinner.show();
    this._UserService.VerifyMobileOtp(d).subscribe((res: any) => {
      debugger
      this.spinner.hide();
      if (res.length == 1) {
        localStorage.setItem('UserId', res[0].userId);
        localStorage.setItem('Username', res[0].name);
        localStorage.setItem('email', res[0].emailId);
        localStorage.setItem('phone', res[0].mobile);
        localStorage.setItem('token', res[0].token);
        window.open('/Home', '_parent');
      } else if (res.length == 0) {
        this._ToastrService.error('Invalid OTP');
        return false;
      } else {
        this._ToastrService.error('Exception Error');
        return false;
      }
    });
  }

  SendOTP() {
    debugger
    if (this.Mobile == '' || this.Mobile == undefined) {
      this._ToastrService.error("Mobile No is required.");
      return false;
    }
    else {
      let obj = {
        Mobile: this.Mobile
      };
      this.spinner.show();
      this._UserService.CheckMobileAllReadyRegisteredOrNot(obj).subscribe(res => {
        this.spinner.hide();
        if (res == 0) {
          this._ToastrService.error("Mobile no doesn't exists");
        }
        else if (res == -1) {
          this._ToastrService.error("Something went wrong. please try again");
        }
        else {
          this.IsOtpSend = true;
        }
      });
    }
  }

}
