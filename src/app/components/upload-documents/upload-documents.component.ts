import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-upload-documents',
  templateUrl: './upload-documents.component.html',
  styleUrls: ['./upload-documents.component.css']
})
export class UploadDocumentsComponent implements OnInit {
  DownloadPath = environment.DownloadPath + '/' + Number(localStorage.getItem('UserId'));
  DocumentForm: FormGroup;
  IsSubmit = false;
  lstDoc: any;
  lstUploadedDoc: any;
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private _userService: UserService,
    private spinner: NgxSpinnerService
  ) {
    this.DocumentForm = this.fb.group({
      userId: Number(localStorage.getItem('UserId')),
      name: ['', Validators.required],
      fileSource: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this._userService.GetLookupDoc().subscribe(res => {
      this.lstDoc = res;
    });
    this.LoadDoc();
  }

  get f() {
    return this.DocumentForm.controls;
  }

  onFileChange(event) {
    var images = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          debugger
          images.push(event.target.result);
          // this.previewUrl = event.target.result;
          this.DocumentForm.patchValue({
            fileSource: images
          });
          this.DocumentForm.updateValueAndValidity();
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  onSubmit() {
    debugger
    this.IsSubmit = true;
    if (this.DocumentForm.invalid) {
      this.DocumentForm.markAllAsTouched();
      this.toastr.error("please, Upload files.");
      return;
    }
    else {
      this.spinner.show();
      this._userService.UploadDoc(this.DocumentForm.value).subscribe(res => {
        this.DocumentForm = this.fb.group({
          userId: Number(localStorage.getItem('UserId')),
          name: ['', Validators.required],
          fileSource: ['', Validators.required]
        });
        this.spinner.hide();
        this.toastr.success('File has been uploaded successfully.');
        this.lstUploadedDoc = res;
      });
    }
  }

  LoadDoc() {
    let obj = {
      userId: Number(localStorage.getItem('UserId'))
    };
    this._userService.DocPath(obj).subscribe(res => {
      debugger
      this.lstUploadedDoc = res;
      this.spinner.hide();
    });
  }

}
