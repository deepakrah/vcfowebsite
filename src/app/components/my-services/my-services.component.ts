import { DatePipe } from '@angular/common';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
// import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ServicesService } from 'src/app/shared/services/services.service';
import { UserService } from 'src/app/shared/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-services',
  templateUrl: './my-services.component.html',
  styleUrls: ['./my-services.component.css']
})
export class MyServicesComponent implements OnInit {
  lstOrders: any = [];
  LoggedInUserId = Number(localStorage.getItem('UserId'));
  SelectedOrderId: any;
  DownloadPath = environment.DownloadPath + '/' + Number(localStorage.getItem('UserId'));
  DocumentForm: FormGroup;
  IsSubmit = false;
  lstDoc: any;
  lstUploadedDoc: any;
  modalRefView: BsModalRef;
  closeResult: string;

  constructor(
    private _servicesService: ServicesService,
    private _userService: UserService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private _datePipe: DatePipe,
    private router: Router,
    private modalService: BsModalService,
  ) {

    let obj = {
      userId: this.LoggedInUserId
    }
    this.spinner.show();
    this._servicesService.GetOrderByUserId(obj).subscribe(res => {
      this.spinner.hide();
      this.lstOrders = res;
      this._userService.GetLookupDoc().subscribe(res => {
        this.lstDoc = res;
      });
    });

    this.DocumentForm = this.formBuilder.group({
      userId: Number(localStorage.getItem('UserId')),
      name: ['', Validators.required],
      fileSource: ['', Validators.required],
      orderId: [this.SelectedOrderId]
    });
  }

  ngOnInit(): void {
  }

  get f() {
    return this.DocumentForm.controls;
  }

  onFileChange(event) {
    var images = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          debugger
          images.push(event.target.result);
          // this.previewUrl = event.target.result;
          this.DocumentForm.patchValue({
            fileSource: images
          });
          this.DocumentForm.updateValueAndValidity();
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  onSubmit() {
    debugger
    this.IsSubmit = true;
    if (this.DocumentForm.invalid) {
      this.DocumentForm.markAllAsTouched();
      this.toastr.error("please, Upload files.");
      return;
    }
    else {
      this.spinner.show();
      const orderId = this.DocumentForm.get('orderId');
      orderId.setValue(this.SelectedOrderId);
      orderId.updateValueAndValidity();

      this._userService.UploadDoc(this.DocumentForm.value).subscribe(res => {
        this.DocumentForm = this.formBuilder.group({
          userId: Number(localStorage.getItem('UserId')),
          name: ['', Validators.required],
          fileSource: ['', Validators.required],
          orderId: [this.SelectedOrderId]
        });
        this.spinner.hide();
        this.toastr.success('File has been uploaded successfully.');
        this.lstUploadedDoc = res;
      });
    }
  }

  openModalUploadDoc(template: TemplateRef<any>, _lst: any) {
    this.IsSubmit = false;
    this.DocumentForm = this.formBuilder.group({
      userId: Number(localStorage.getItem('UserId')),
      name: ['', Validators.required],
      fileSource: ['', Validators.required],
      orderId: [this.SelectedOrderId]
    });
    debugger
    this.SelectedOrderId = Number(_lst.id);
    let obj = {
      userId: Number(localStorage.getItem('UserId')),
      OrderId: Number(_lst.id)
    };
    this._userService.DocPath(obj).subscribe(res => {
      debugger
      this.lstUploadedDoc = res;
      this.modalRefView = this.modalService.show(template, { ignoreBackdropClick: true, keyboard: true, class: 'modal-lg' });
      this.spinner.hide();
    });


  }

}
