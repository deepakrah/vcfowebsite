import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ContactusService } from 'src/app/shared/services/contactus.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  ContactUsForm: FormGroup;
  submitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private _ContactusService: ContactusService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.ContactUsForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  get f() { return this.ContactUsForm.controls; }

  Save() {
    this.submitted = true;
    if (this.ContactUsForm.invalid) {
      this.toastr.error('Please fill in all the * required fields.');
      return;
    }
    else {
      this.spinner.show();
      this._ContactusService.SaveContactUs(this.ContactUsForm.value).subscribe(res => {
        this.spinner.hide();
        this.toastr.success("Your request has been submitted. We will contact you sortly.");
        this.submitted = false;
        this.ContactUsForm.reset();
      });
    }
  }

}
