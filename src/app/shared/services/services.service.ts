import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  private BASE_API_URL = environment.apiUrl;
  private _controllerName: string = "Services/";
  private _url: string = this.BASE_API_URL + this._controllerName;
  private _methodName: string = "";
  private _param: {};
  constructor(private _http: HttpClient) { }

  GetAllServices(): Observable<any> {
    this._methodName = "GetAllServices/";
    this._param = {};
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  SaveServices(_obj: any): Observable<any> {
    this._methodName = "SaveServices/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  GetServicesById(_obj: any): Observable<any> {
    this._methodName = "GetServicesById/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }
  
  GetServicesByCode(_obj: any): Observable<any> {
    this._methodName = "GetServicesByCode/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  GetOrderByUserId(_obj: any): Observable<any> {
    this._methodName = "GetOrderByUserId/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }
}
