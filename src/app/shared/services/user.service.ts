import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private BASE_API_URL = environment.apiUrl;
  private _controllerName: string = "Admin/";
  private _url: string = this.BASE_API_URL + this._controllerName;
  private _methodName: string = "";
  private _param: {};
  constructor(private _http: HttpClient) { }

  SaveUser(_obj: any): Observable<any> {
    this._methodName = "SaveUser/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  addOrder(_obj: any): Observable<any> {
    this._methodName = "addOrder/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  CheckMobileAllReadyRegisteredOrNot(_obj: any): Observable<any> {
    this._methodName = "CheckMobileAllReadyRegisteredOrNot/";
    this._param = _obj;
    return this._http.post<any>(
      this._url + this._methodName, this._param
    );
  }

  VerifyMobileOtp(_obj: any): Observable<any[]> {
    this._methodName = "VerifyMobileOtp/";
    this._param = _obj;
    return this._http.post<any[]>(
      this._url + this._methodName, this._param
    );
  }

  GetLookupDoc(): Observable<any[]> {
    this._methodName = "GetLookupDoc?";
    this._param = '';
    return this._http.get<any[]>(
      this._url + this._methodName+ this._param
    );
  }

  UploadDoc(_obj: any): Observable<any[]> {
    this._methodName = "UploadDoc/";
    this._param = _obj;
    return this._http.post<any[]>(
      this._url + this._methodName, this._param
    );
  }

  DocPath(_obj: any): Observable<any[]> {
    this._methodName = "DocPath/";
    this._param = _obj;
    return this._http.post<any[]>(
      this._url + this._methodName, this._param
    );
  }
}
