import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private httpClient: HttpClient) {

  }
  headers = new HttpHeaders();
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })

  };

  get(method) {
    return this.httpClient.get(method);
  }
  getWithParam(method, _params) {
    return this.httpClient.get(method, _params);
  }
  post(method, data) {
    return this.httpClient.post(method, data);
  }
  put(method, data) {
    return this.httpClient.put(method, data);
  }

  
}
