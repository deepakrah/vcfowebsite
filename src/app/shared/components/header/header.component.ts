import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  rows: any[];
  IsLogin = false;
  constructor(
    private _servicesService: ServicesService,
  ) {
    if (localStorage.getItem('UserId') != '' && localStorage.getItem('UserId') != undefined)
      this.IsLogin = true;
  }

  ngOnInit(): void {
    this.LoadData();
  }

  LoadData() {
    // this._servicesService.GetAllServices().subscribe(res => {
    //   this.rows = res;
    // });
  }

  Logout() {
    localStorage.removeItem('UserId');
    localStorage.removeItem('Username');
    localStorage.removeItem('email');
    localStorage.removeItem('phone');
    localStorage.removeItem('token');
    window.open('/Home', '_parent');
  }
}
