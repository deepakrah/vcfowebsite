import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sidebar-side',
  templateUrl: './sidebar-side.component.html',
  styleUrls: ['./sidebar-side.component.css']
})
export class SidebarSideComponent implements OnInit {
  form: FormGroup;
  isSubmit;
  constructor(
    private router: Router,
    private _userService: UserService,
    private spinner: NgxSpinnerService,
    private _formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.form = this._formBuilder.group({
      name: ['', Validators.required],
      emailId: ['', Validators.required],
      mobile: ['', Validators.required]
    });
  }

  get f() {
    return this.form.controls;
  }

  CreateRegistration() {
    if (this.form.invalid) {
      this.toastr.error('Please fill in all the * required fields.');
      return;
    }
    this.isSubmit = true;
    if (this.form.valid) {
      this.spinner.show();
      this._userService.SaveUser(this.form.value).subscribe(res => {
        debugger
        this.isSubmit = false;
        this.spinner.hide();
        localStorage.setItem('UserId', res[0].userId);
        localStorage.setItem('Username', res[0].name);
        localStorage.setItem('email', res[0].emailId);
        localStorage.setItem('phone', res[0].mobile);
        localStorage.setItem('token', res[0].token);
        this.toastr.success('Service has been added sueccessfully.');
        // this.router.navigateByUrl('/Contactus');
        window.open('/Contactus','_parent');
      });
    }
  }

}
