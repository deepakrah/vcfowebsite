import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ServiceDetailsComponent } from './components/service-details/service-details.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrivateLimitedCompanyComponent } from './components/Servicetemplate/private-limited-company/private-limited-company.component';
import { LimitedLiabilityPartnershipComponent } from './components/Servicetemplate/limited-liability-partnership/limited-liability-partnership.component';
import { CallActionComponent } from './shared/components/call-action/call-action.component';
import { SidebarSideComponent } from './shared/components/sidebar-side/sidebar-side.component';
import { OnePersonCompanyComponent } from './components/Servicetemplate/one-person-company/one-person-company.component';
import { SoleProprietorshipComponent } from './components/Servicetemplate/sole-proprietorship/sole-proprietorship.component';
import { PartnershipFirmComponent } from './components/Servicetemplate/partnership-firm/partnership-firm.component';
import { DrugLicenseComponent } from './components/Servicetemplate/drug-license/drug-license.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { FSSAIRegistrationComponent } from './components/Servicetemplate/fssairegistration/fssairegistration.component';
import { ImporterExporterCodeIndiaComponent } from './components/Servicetemplate/importer-exporter-code-india/importer-exporter-code-india.component';
import { ISOCertificateComponent } from './components/Servicetemplate/isocertificate/isocertificate.component';
import { TradeLicenseComponent } from './components/Servicetemplate/trade-license/trade-license.component';
import { CopyrightRegistrationComponent } from './components/Servicetemplate/copyright-registration/copyright-registration.component';
import { PatentRegistrationComponent } from './components/Servicetemplate/patent-registration/patent-registration.component';
import { ProvisionalPatentComponent } from './components/Servicetemplate/provisional-patent/provisional-patent.component';
import { PartnershipFirmTaxReturnFilingComponent } from './components/Servicetemplate/partnership-firm-tax-return-filing/partnership-firm-tax-return-filing.component';
import { ProprietorshipReturnFilingComponent } from './components/Servicetemplate/proprietorship-return-filing/proprietorship-return-filing.component';
import { TDSComponent } from './components/Servicetemplate/tds/tds.component';
import { ContactusComponent } from './components/contactus/contactus.component';

import { AppLayoutComponent } from './components/layout/app-layout/app-layout.component';
import { TrademarkRegistrationComponent } from './components/Servicetemplate/trademark-registration/trademark-registration.component';
import { TrademarkObjectionComponent } from './components/Servicetemplate/trademark-objection/trademark-objection.component';
import { TrademarkOppositionComponent } from './components/Servicetemplate/trademark-opposition/trademark-opposition.component';
import { TrademarkRenewalComponent } from './components/Servicetemplate/trademark-renewal/trademark-renewal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule, DatePipe } from '@angular/common';
import { SuccessComponent } from './components/success/success.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';
import { AuthInterceptor } from './Auth/auth-interceptor';
import { LoginComponent } from './components/login/login.component';
import { MyServicesComponent } from './components/my-services/my-services.component';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { FileGSTReturnEasilyComponent } from './components/Servicetemplate/file-gstreturn-easily/file-gstreturn-easily.component';
import { GSTAnnualReturnComponent } from './components/Servicetemplate/gstannual-return/gstannual-return.component';
import { GSTInputTaxCreditReconciliationComponent } from './components/Servicetemplate/gstinput-tax-credit-reconciliation/gstinput-tax-credit-reconciliation.component';
import { GSTInvoicingComponent } from './components/Servicetemplate/gstinvoicing/gstinvoicing.component';
import { GSTRegistrationComponent } from './components/Servicetemplate/gstregistration/gstregistration.component';
import { PayrollComponent } from './components/Servicetemplate/payroll/payroll.component';
import { PFRegistrationComponent } from './components/Servicetemplate/pfregistration/pfregistration.component';
import { PFReturnFilingComponent } from './components/Servicetemplate/pfreturn-filing/pfreturn-filing.component';
import { ESIRegistrationComponent } from './components/Servicetemplate/esiregistration/esiregistration.component';
import { RegisteredOfficeChangeComponent } from './components/Servicetemplate/registered-office-change/registered-office-change.component';
import { AddDirectorsComponent } from './components/Servicetemplate/add-directors/add-directors.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ServiceDetailsComponent,
    PrivateLimitedCompanyComponent,
    LimitedLiabilityPartnershipComponent,
    CallActionComponent,
    SidebarSideComponent,
    OnePersonCompanyComponent,
    SoleProprietorshipComponent,
    PartnershipFirmComponent,
    DrugLicenseComponent,
    FSSAIRegistrationComponent,
    ImporterExporterCodeIndiaComponent,
    ISOCertificateComponent,
    TradeLicenseComponent,
    CopyrightRegistrationComponent,
    PatentRegistrationComponent,
    ProvisionalPatentComponent,
    PartnershipFirmTaxReturnFilingComponent,
    ProprietorshipReturnFilingComponent,
    TDSComponent,
    ContactusComponent,

    AppLayoutComponent,
    TrademarkRegistrationComponent,
    TrademarkObjectionComponent,
    TrademarkOppositionComponent,
    TrademarkRenewalComponent,
    SuccessComponent,
    UploadDocumentsComponent,
    LoginComponent,
    MyServicesComponent,
    FileGSTReturnEasilyComponent,
    GSTAnnualReturnComponent,
    GSTInputTaxCreditReconciliationComponent,
    GSTInvoicingComponent,
    GSTRegistrationComponent,
    PayrollComponent,
    PFRegistrationComponent,
    PFReturnFilingComponent,
    ESIRegistrationComponent,
    RegisteredOfficeChangeComponent,
    AddDirectorsComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
