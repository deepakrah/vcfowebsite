import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ServiceDetailsComponent } from './components/service-details/service-details.component';
import { CopyrightRegistrationComponent } from './components/Servicetemplate/copyright-registration/copyright-registration.component';
import { DrugLicenseComponent } from './components/Servicetemplate/drug-license/drug-license.component';
import { FSSAIRegistrationComponent } from './components/Servicetemplate/fssairegistration/fssairegistration.component';
import { ImporterExporterCodeIndiaComponent } from './components/Servicetemplate/importer-exporter-code-india/importer-exporter-code-india.component';
import { ISOCertificateComponent } from './components/Servicetemplate/isocertificate/isocertificate.component';
import { LimitedLiabilityPartnershipComponent } from './components/Servicetemplate/limited-liability-partnership/limited-liability-partnership.component';
import { OnePersonCompanyComponent } from './components/Servicetemplate/one-person-company/one-person-company.component';
import { PartnershipFirmTaxReturnFilingComponent } from './components/Servicetemplate/partnership-firm-tax-return-filing/partnership-firm-tax-return-filing.component';
import { PartnershipFirmComponent } from './components/Servicetemplate/partnership-firm/partnership-firm.component';
import { PatentRegistrationComponent } from './components/Servicetemplate/patent-registration/patent-registration.component';
import { PrivateLimitedCompanyComponent } from './components/Servicetemplate/private-limited-company/private-limited-company.component';
import { ProprietorshipReturnFilingComponent } from './components/Servicetemplate/proprietorship-return-filing/proprietorship-return-filing.component';
import { ProvisionalPatentComponent } from './components/Servicetemplate/provisional-patent/provisional-patent.component';
import { SoleProprietorshipComponent } from './components/Servicetemplate/sole-proprietorship/sole-proprietorship.component';
import { TDSComponent } from './components/Servicetemplate/tds/tds.component';
import { TradeLicenseComponent } from './components/Servicetemplate/trade-license/trade-license.component';
import { ContactusComponent } from './components/contactus/contactus.component';
import { AppLayoutComponent } from './components/layout/app-layout/app-layout.component';
import { TrademarkRegistrationComponent } from './components/Servicetemplate/trademark-registration/trademark-registration.component';
import { TrademarkObjectionComponent } from './components/Servicetemplate/trademark-objection/trademark-objection.component';
import { TrademarkOppositionComponent } from './components/Servicetemplate/trademark-opposition/trademark-opposition.component';
import { TrademarkRenewalComponent } from './components/Servicetemplate/trademark-renewal/trademark-renewal.component';
import { SuccessComponent } from './components/success/success.component';
import { UploadDocumentsComponent } from './components/upload-documents/upload-documents.component';
import { LoginComponent } from './components/login/login.component';
import { MyServicesComponent } from './components/my-services/my-services.component';
import { FileGSTReturnEasilyComponent } from './components/Servicetemplate/file-gstreturn-easily/file-gstreturn-easily.component';
import { GSTAnnualReturnComponent } from './components/Servicetemplate/gstannual-return/gstannual-return.component';
import { GSTInputTaxCreditReconciliationComponent } from './components/Servicetemplate/gstinput-tax-credit-reconciliation/gstinput-tax-credit-reconciliation.component';
import { GSTInvoicingComponent } from './components/Servicetemplate/gstinvoicing/gstinvoicing.component';
import { GSTRegistrationComponent } from './components/Servicetemplate/gstregistration/gstregistration.component';
import { PayrollComponent } from './components/Servicetemplate/payroll/payroll.component';
import { PFRegistrationComponent } from './components/Servicetemplate/pfregistration/pfregistration.component';
import { PFReturnFilingComponent } from './components/Servicetemplate/pfreturn-filing/pfreturn-filing.component';
import { ESIRegistrationComponent } from './components/Servicetemplate/esiregistration/esiregistration.component';
import { AddDirectorsComponent } from './components/Servicetemplate/add-directors/add-directors.component';
import { RegisteredOfficeChangeComponent } from './components/Servicetemplate/registered-office-change/registered-office-change.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: '', component: AppLayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'Home', component: HomeComponent },
      // { path: 'ServiceDetails/:id', component: ServiceDetailsComponent },
      { path: 'PrivateLimitedCompany', component: PrivateLimitedCompanyComponent },
      { path: 'LimitedLiabilityPartnership', component: LimitedLiabilityPartnershipComponent },
      { path: 'OnePersonCompany', component: OnePersonCompanyComponent },
      { path: 'SoleProprietorship', component: SoleProprietorshipComponent },
      { path: 'PartnershipFirm', component: PartnershipFirmComponent },
      { path: 'DrugLicense', component: DrugLicenseComponent },
      { path: 'FSSAIRegistration', component: FSSAIRegistrationComponent },
      { path: 'ImporterExporterCodeIndia', component: ImporterExporterCodeIndiaComponent },
      { path: 'ISOCertificate', component: ISOCertificateComponent },
      { path: 'TradeLicense', component: TradeLicenseComponent },
      { path: 'CopyrightRegistration', component: CopyrightRegistrationComponent },
      { path: 'PatentRegistration', component: PatentRegistrationComponent },
      { path: 'ProvisionalPatent', component: ProvisionalPatentComponent },
      { path: 'PartnershipFirmTaxReturnFiling', component: PartnershipFirmTaxReturnFilingComponent },
      { path: 'ProprietorshipReturnFiling', component: ProprietorshipReturnFilingComponent },
      { path: 'TDS', component: TDSComponent },
      { path: 'TrademarkRegistration', component: TrademarkRegistrationComponent },
      { path: 'TrademarkObjection', component: TrademarkObjectionComponent },
      { path: 'TrademarkOpposition', component: TrademarkOppositionComponent },
      { path: 'TrademarkRenewal', component: TrademarkRenewalComponent },
      { path: 'Contactus', component: ContactusComponent },
      { path: 'success', component: SuccessComponent },
      { path: 'uploadDocuments', component: UploadDocumentsComponent },
      { path: 'myServices', component: MyServicesComponent },
      { path: 'FileGSTReturnEasily', component: FileGSTReturnEasilyComponent },
      { path: 'GSTAnnualReturn', component: GSTAnnualReturnComponent },
      { path: 'GSTInputTaxCreditReconciliation', component: GSTInputTaxCreditReconciliationComponent },
      { path: 'GSTInvoicing', component: GSTInvoicingComponent },
      { path: 'GSTRegistration', component: GSTRegistrationComponent },
      { path: 'Payroll', component: PayrollComponent },
      { path: 'PFRegistration', component: PFRegistrationComponent },
      { path: 'PFReturnFiling', component: PFReturnFilingComponent },
      { path: 'ESIRegistration', component: ESIRegistrationComponent },
      { path: 'RegisteredOfficeChange', component: RegisteredOfficeChangeComponent },
      { path: 'AddDirectors', component: AddDirectorsComponent }
    ],

  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
